FROM python:3.9
COPY requirements.txt requirements.txt
RUN pip install --no-cache-dir -r requirements.txt

COPY . /project
WORKDIR /project

EXPOSE 8080

CMD ["uvicorn", "main.main:app","--host", "0.0.0.0", "--port", "8080"]

